ExUnit.configure(formatters: [JUnitFormatter, ExUnit.CLIFormatter])
ExUnit.start(exclude: [:run_script])
Ecto.Adapters.SQL.Sandbox.mode(ExampleApp.Repo, :manual)
